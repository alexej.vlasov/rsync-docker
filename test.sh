#!/usr/bin/env bash

docker build -t registry.gitlab.com/alexej.vlasov/rsync-docker/rsync:3.0.9-patched .

MASTERDIR=/Users/alexey_vlasov/temp/kafka_2.12-2.3.0/
SLAVEDIR=/Users/alexey_vlasov/temp/ksd/

BACKUPDIR=/Users/alexey_vlasov/temp/ksd-backup/

DATE=`date '+%Y-%m-%d_%H-%M-%S'`

docker run -v $MASTERDIR:/master -v $SLAVEDIR:/slave -v $BACKUPDIR:/backup registry.gitlab.com/alexej.vlasov/rsync-docker/rsync:3.0.9-patched \
  rsync -avv --detect-renamed --dry-run --checksum --delete-delay  /master/ /slave/
