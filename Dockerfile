FROM  golang:1.13.5-alpine3.10 as build_stage

WORKDIR /
RUN apk add git
RUN git clone https://gitlab.com/ambrevar/hsync.git
WORKDIR /hsync
RUN go build -ldflags "-X main.version=$(git describe --tags --always)"


FROM alpine:3.11

COPY --from=build_stage /hsync/hsync /bin/hsync


